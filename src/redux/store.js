import {configureStore, getDefaultMiddleware} from '@reduxjs/toolkit';
import {logger} from 'redux-logger';
import {rootReducer} from './root.reducer';
export const configureAppStore = preloadedState => {
  const store = configureStore({
    reducer: rootReducer,
    middleware: [logger, ...getDefaultMiddleware()],
    devTools: true,
    preloadedState: preloadedState,
  });

  if (process.env.NODE_ENV !== 'production') {
  }

  return store;
};
