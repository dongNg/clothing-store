export {default as HomePage} from './home-page/HomePage';
export {default as ShopPage} from './shop-page/ShopPage';
export {default as SignPage} from './sign-page/SignPage';
