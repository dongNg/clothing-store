import {useState} from 'react';
import {SignIn, SignUp} from '../../components';

const SignPage = () => {
  const [tabs, setTabs] = useState([
    {
      id: 1,
      title: 'Sign In',
      isActive: true,
      keyValue: 'in',
    },
    {
      id: 2,
      title: 'Sign Up',
      isActive: false,
      keyValue: 'up',
    },
  ]);

  const [selectTab, setSelectTab] = useState(tabs[0]);

  const handleChangeTab = id => {
    const vTab = tabs.map(x => {
      if (x.id === id) {
        x.isActive = true;
        setSelectTab(x);
      } else {
        x.isActive = false;
      }
      return x;
    });
    setTabs(vTab);
  };
  return (
    <section className="section is-medium">
      <div className="is-flex is-justify-content-space-around">
        <div className="box">
          <div className="tabs is-large is-fullwidth">
            <ul>
              {tabs.map(({id, title, isActive, icon, keyValue}) => (
                <li
                  key={id}
                  className={`${isActive ? 'is-active' : ''}`}
                  onClick={() => handleChangeTab(id)}
                >
                  <a href={`#${keyValue}`}>
                    <span>{title}</span>
                  </a>
                </li>
              ))}
            </ul>
          </div>
          {selectTab.keyValue === 'in' && selectTab.isActive ? (
            <SignIn />
          ) : (
            <SignUp />
          )}
        </div>
      </div>
    </section>
  );
};

export default SignPage;
