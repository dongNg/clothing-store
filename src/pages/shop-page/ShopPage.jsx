import {CollectionPreview} from '../../components';
import {default as SHOP_DATA} from './shop.data';
import {useState} from 'react';
const ShopPage = () => {
  // eslint-disable-next-line
  const [collections, setCollections] = useState(SHOP_DATA);
  return (
    <div>
      {collections.map(({id, ...otherCollectionProps}) => (
        <CollectionPreview key={id} {...otherCollectionProps} />
      ))}
    </div>
  );
};

export default ShopPage;
