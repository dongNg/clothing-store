import './Modal.scss';
const Modal = ({open}) => {
  return (
    <div className={`modal ${open ? 'is-active' : ''}`}>
      <div className="modal-background" />
      <div className="modal-content"></div>
      <button className="modal-close is-large" aria-label="close"></button>
    </div>
  );
};

export default Modal;
