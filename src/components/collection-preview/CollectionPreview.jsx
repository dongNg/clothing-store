import {CollectionItem} from '..';
import './CollectionPreview.scss';
const CollectionPreview = ({title, items}) => {
  return (
    <section className="section">
      <div className="collection-preview">
        <h1 className="title">{title}</h1>
        <div className="preview">
          <div className="columns is-multiline is-3">
            {items
              ?.filter((item, idx) => idx < 4)
              .map(({id, ...otherItemProps}) => (
                <div className="column is-one-quarter" key={id}>
                  <CollectionItem {...otherItemProps} />
                </div>
              ))}
          </div>
        </div>
      </div>
    </section>
  );
};

export default CollectionPreview;
