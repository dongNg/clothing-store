import './CollectionItem.scss';
const CollectionItem = ({imageUrl, name, price}) => {
  return (
    <div className="card card--collection-item">
      <div className="card-image">
        <figure className="image is-square">
          <img src={imageUrl} alt={name} />
        </figure>
      </div>
      <div className="card-content">
        <div className="content has-text-centered">
          <h1 className="title is-4 is-text-overflow is-uppercase">{name}</h1>
          <span className="subtitle">${price}</span>
        </div>
      </div>
      <div className="card-footer has-background-black has-text-primary-light card--collection-footer">
        <div className="card-footer-item is-uppercase is-clickable">Add to cart</div>
      </div>
    </div>
  );
};

export default CollectionItem;
