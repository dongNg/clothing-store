import {ReactComponent as Logo} from '../../assets/crown.svg';
import {Link} from 'react-router-dom';
const Header = () => {
  return (
    <nav
      className="navbar is-transparent"
      role="navigation"
      aria-label="main navigation"
    >
      <div className="navbar-brand">
        <Link to="/" className="navbar-item">
          <Logo />
        </Link>
        <a
          role="button"
          href="#a"
          className="navbar-burger"
          aria-label="menu"
          aria-expanded="false"
          data-target="navbarBasicExample"
        >
          <span aria-hidden="true" />
          <span aria-hidden="true" />
          <span aria-hidden="true" />
        </a>
      </div>
      <div id="navbarBasicExample" className="navbar-menu">
        <div className="navbar-start">
          <Link to="/shop" className="navbar-item">
            SHOP
          </Link>
          <Link to="/shop" className="navbar-item">
            CONTACT
          </Link>
        </div>
        <div className="navbar-end">
          <Link to="/signin" className="navbar-item">
            SIGN IN
          </Link>
        </div>
      </div>
    </nav>
  );
};

export default Header;
