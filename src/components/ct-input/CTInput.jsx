import './CTInput.scss';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';

const inputHorizontal = (label, otherProps) => (
  <div className="field is-horizontal">
    <div className="field-label is-normal">
      <label className="label">{label}</label>
    </div>
    <div className="field-body">
      <div className="field">
        <p className="control">
          <input className="input" {...otherProps} />
        </p>
      </div>
    </div>
  </div>
);

const inputDefault = (
  label,
  iconLeft,
  iconRight,
  isSuccess,
  msgSuccess,
  isError,
  msgError,
  isLoad,
  otherProps,
) => (
  <div className="field">
    <label className="label">{label}</label>
    <div
      className={`control ${isLoad ? 'is-loading' : ''} ${
        iconLeft ? 'has-icons-left' : ''
      } ${iconRight ? 'has-icons-right' : ''}`}
    >
      <input className="input" {...otherProps} />
      {iconLeft ? (
        <span className="icon is-left">
          <FontAwesomeIcon icon={iconLeft} />
        </span>
      ) : (
        ''
      )}
      {iconRight ? (
        <span className="icon is-right">
          <FontAwesomeIcon icon={iconRight} />
        </span>
      ) : (
        ''
      )}

      {isSuccess ? <p class="help is-success">{msgSuccess}</p> : ''}
      {isError ? <p class="help is-danger">{msgError}</p> : ''}
    </div>
  </div>
);
const CTInput = ({
  isHorizontal,
  label,
  iconLeft,
  iconRight,
  isSuccess,
  msgSuccess,
  isError,
  msgError,
  isLoad,
  ...otherProps
}) => {
  return (
    <>
      {isHorizontal
        ? inputHorizontal(label, otherProps)
        : inputDefault(
            label,
            iconLeft,
            iconRight,
            isSuccess,
            msgSuccess,
            isError,
            msgError,
            isLoad,
            otherProps,
          )}
    </>
  );
};

export default CTInput;
