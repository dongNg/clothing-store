import './SignIn.scss';
import {CTButton, CTInput} from '../index';
import {faEnvelope, faLock} from '@fortawesome/free-solid-svg-icons';
const SignIn = () => {
  return (
    <form className="form-sign">
      <h1 className="title has-text-primary-dark has-text-centered">Sign In</h1>
      <CTInput
        label="Email"
        type="email"
        placeholder="Email"
        iconLeft={faEnvelope}
      />
      <CTInput
        label="Password"
        type="password"
        placeholder="Password"
        iconLeft={faLock}
      />
      <CTButton className="is-info" children="Submit" type="submit" />
    </form>
  );
};

export default SignIn;
