import './MenuItem.scss';
const MenuItem = ({imageUrl, title}) => {
  return (
    <div className="clothing-menu-item">
      <div
        className="background-image"
        style={{backgroundImage: `url(${imageUrl})`}}
      ></div>
      <div className="content">
        <h1 className="title is-uppercase">{title}</h1>
        <span className="subtitle">SHOW NOW</span>
      </div>
    </div>
  );
};

export default MenuItem;
