import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import './CTButton.scss';
const CTButton = ({
  children,
  className,
  isLoad,
  icon,
  ...otherProps
}) => {
  return (
    <button
      className={`button ${className} ${isLoad ? 'is-loading' : ''}`}
      {...otherProps}
    >
      {icon ? (
        <span class="icon">
          <FontAwesomeIcon icon={icon} />
        </span>
      ) : (
        ''
      )}
      <span>{children}</span>
    </button>
  );
};

export default CTButton;
