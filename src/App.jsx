import {Route, Switch} from 'react-router-dom';
import {Header} from './components/index';
import './App.scss';
import {HomePage, ShopPage, SignPage} from './pages';

function App() {
  return (
    <div className="container">
      <Header />
      <main>
        <Switch>
          <Route exact path="/" component={HomePage} />
          <Route exact path="/shop" component={ShopPage} />
          <Route exact path="/signin" component={SignPage} />
        </Switch>
      </main>
    </div>
  );
}

export default App;
